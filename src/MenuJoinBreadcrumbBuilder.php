<?php

namespace Drupal\menu_join;

use Drupal\context\ContextManager;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Build breadcrumbs based on menu_join_active_trail from context.
 */
class MenuJoinBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The context manager.
   *
   * @var \Drupal\context\ContextManager
   */
  protected $contextManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The active trail.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $activeTrail;

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $linkManager;

  /**
   * The title resolver.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected $titleResolver;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Configuration of the context reaction affecting breadcrumbs.
   *
   * @var array
   */
  protected $configuration;

  /**
   * Current menu name, used in iteration.
   *
   * @var string
   */
  protected $currentMenuName = '';

  /**
   * Our menu list.
   *
   * @var array
   */
  protected $menuList = [];

  /**
   * Constructor.
   *
   * @param \Drupal\context\ContextManager $context_manager
   *   The context manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $active_trail
   *   The active trail.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $link_manager
   *   The menu link manager.
   * @param \Drupal\Core\Controller\TitleResolverInterface $title_resolver
   *   The title resolver.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
    ContextManager $context_manager,
    EntityTypeManagerInterface $entity_type_manager,
    MenuActiveTrailInterface $active_trail,
    MenuLinkManagerInterface $link_manager,
    TitleResolverInterface $title_resolver,
    RequestStack $request_stack
  ) {
    $this->contextManager = $context_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->activeTrail = $active_trail;
    $this->linkManager = $link_manager;
    $this->titleResolver = $title_resolver;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return count($this->getActiveReactions()) > 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveReactions() {
    return $this->contextManager->getActiveReactions('menu_join');
  }

  /**
   * Returns list of active trails keyed by menu name.
   *
   * @return array
   *   Array of active Trails.
   */
  public function getActiveTrails() {
    $menus = $this->entityTypeManager->getStorage('menu')->loadMultiple();
    $activeTrails = [];
    foreach ($menus as $menu_name => $menu) {
      $at = $this->activeTrail->getActiveTrailIds($menu_name);
      $link_ids = array_filter($at);
      if (!empty($link_ids)) {
        foreach ($link_ids as $link_id) {
          $activeTrails[$menu_name][$link_id] = $link = $this->linkManager->getInstance(['id' => $link_id]);
        }
      }
    }
    return $activeTrails;
  }

  /**
   * Extracts menu machine name from link id.
   *
   * @param string $id
   *   Link id.
   *
   * @return false|string
   *   Menu machine name.
   */
  protected function menuNameFromLinkId($id) {
    return substr($id, 0, strpos($id, ':'));
  }

  /**
   * Orders active menus.
   */
  protected function calculateMenuList() {
    $this->currentMenuName = '';
    $this->menuList = [];
    while ($this->getNextMenu() !== FALSE);
  }

  /**
   * Recursive function used to order active menus.
   *
   * @return bool
   *   Keep iterating.
   */
  protected function getNextMenu() {

    if (!$this->currentMenuName) {
      $this->currentMenuName = '';
      $this->menuList = [];
    }

    // Get the parent menu.
    $contexts = $this->contextManager->getActiveContexts();
    foreach ($contexts as $context) {
      if ($context->hasReaction('menu_join')) {
        $menu_join = $context->getReaction('menu_join');
        $menu_join_conf = $menu_join->getConfiguration();
        if ($menu_join_conf['parent_menu'] === $this->currentMenuName) {
          $active_trail = $context->getReaction('active_trail');
          $active_trail_conf = $active_trail->getConfiguration();
          /** @var \Drupal\menu_link_content\MenuLinkContentInterface $active_trail_link */
          $active_trail_menu = $this->menuNameFromLinkId($active_trail_conf["trail"]);
          $this->currentMenuName = $active_trail_menu;
          $this->menuList[] = $active_trail_menu;
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(['url.path']);

    // Start with home page.
    $breadcrumb->addLink(
      Link::createFromRoute($this->t('Home'), '<front>')
    );

    // Get an ordered list of menus based on menu_join.
    $this->calculateMenuList();

    // If there are active trails not in our ordered list we presume this to be
    // the node's actual menu item and we prepend to the menu list.
    $active_trails = $this->getActiveTrails();
    $all_menus = array_unique($this->menuList + array_keys($active_trails));

    foreach ($all_menus as $menu_name) {
      $links = array_reverse($active_trails[$menu_name]);
      foreach ($links as $link) {
        $breadcrumb->addLink(
          Link::fromTextAndUrl($link->getTitle(), $link->getUrlObject())
        );
      }
    }

    return $breadcrumb;
  }

}
